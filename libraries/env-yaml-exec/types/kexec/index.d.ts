// Type definitions for kexec v3.0.0
// Project: https://github.com/jprichardson/node-kexec
// Definitions by: Alexander Ostapenko <https://github.com/alx13>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped
// TypeScript Version: 2.0

/// <reference types="node"/>

declare module "kexec" {
  function kexec(cmd: string, args: string[]): void;
  export = kexec;
}
