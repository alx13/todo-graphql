"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const fs = tslib_1.__importStar(require("fs"));
const path = tslib_1.__importStar(require("path"));
const yaml = require("js-yaml");
const yargs = require("yargs");
exports.yargs = yargs;
const kexec = require("kexec");
function readEnvFile(cwd, file) {
    try {
        return fs.readFileSync(path.resolve(cwd, (file || 'env.yaml')), 'utf-8');
    }
    catch (e) {
        if (e.code === 'ENOENT' && !file) {
            return '';
        }
        throw e;
    }
}
exports.readEnvFile = readEnvFile;
function checkEnv(env) {
    if (Array.isArray(env)) {
        throw new Error('env file should be an object');
    }
    Object.keys(env).forEach((key) => {
        // tslint:disable-next-line:strict-type-predicates
        if (typeof env[key] !== 'string') {
            throw new Error('env file should contain only strings');
        }
    });
    return env;
}
exports.checkEnv = checkEnv;
function parseEnv(content, root) {
    const env = yaml.safeLoad(content);
    if (!env) {
        throw new Error(`Can't load content to yaml ${content}`);
    }
    if (root) {
        return env[root];
    }
    return env;
}
exports.parseEnv = parseEnv;
function filterEnv(env, force = false) {
    checkEnv(env);
    // tslint:disable-next-line:no-inferred-empty-object-type
    return Object.keys(env)
        .reduce((acc, key) => {
        if (!force && process.env[key]) {
            return Object.assign({}, acc, { [key]: process.env[key] });
        }
        return Object.assign({}, acc, { [key]: env[key] });
    }, {});
}
exports.filterEnv = filterEnv;
function updateEnv(env) {
    Object.keys(env).forEach((key) => {
        process.env[key] = env[key];
    });
}
exports.updateEnv = updateEnv;
function loadEnv(file, root, override) {
    const content = readEnvFile(process.cwd(), file);
    const source = parseEnv(content, root);
    return filterEnv(source, override);
}
exports.loadEnv = loadEnv;
function loadAndUpdateEnv(files, root, override) {
    if (!files) {
        const env = loadEnv(undefined, root, override);
        updateEnv(env);
        return;
    }
    files.forEach((file) => {
        const env = loadEnv(file, root, override);
        updateEnv(env);
    });
}
exports.loadAndUpdateEnv = loadAndUpdateEnv;
function exec(cmdAndArgs) {
    const [cmd, ...args] = cmdAndArgs;
    kexec(cmd, args);
}
exports.exec = exec;
//# sourceMappingURL=lib.js.map