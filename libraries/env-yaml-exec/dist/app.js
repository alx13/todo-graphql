#!/usr/bin/env node
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const yargs = tslib_1.__importStar(require("yargs"));
const fs = tslib_1.__importStar(require("fs"));
const path = tslib_1.__importStar(require("path"));
const pkg = JSON.parse(fs.readFileSync(path.join(__dirname, '../package.json'), 'utf-8'));
const lib_1 = require("./lib");
function parseArgs(args) {
    return yargs
        .usage('Read .env and exec process\nUsage: $0 [options] <cmd> <args>')
        .version((pkg).version)
        .options({
        env: {
            alias: 'E',
            demandOption: false,
            describe: 'Location of .env file relative to current working directory',
            type: 'string',
        },
        force: {
            alias: 'f',
            default: false,
            demandOption: false,
            describe: 'Override existing env variables from .env file',
            type: 'boolean',
        },
        root: {
            alias: 'r',
            default: undefined,
            demandOption: false,
            describe: 'root key in yaml',
            type: 'string',
        },
    })
        .demandCommand(1)
        // tslint:disable-next-line:no-any
        .parse(args);
}
exports.parseArgs = parseArgs;
function run() {
    const args = parseArgs(process.argv.slice(2));
    if (Array.isArray(args.env)) {
        lib_1.loadAndUpdateEnv(args.env, args.root, args.force);
    }
    else if (args.env) {
        lib_1.loadAndUpdateEnv([args.env], args.root, args.force);
    }
    else {
        lib_1.loadAndUpdateEnv(undefined, args.root, args.force);
    }
    lib_1.exec(args._);
}
exports.run = run;
if (require.main === module) {
    run();
}
//# sourceMappingURL=app.js.map