#!/usr/bin/env node
export interface CLIOptions {
    _: string[];
    env?: string[];
    root?: string;
    force: boolean;
}
export declare function parseArgs(args: string[]): CLIOptions;
export declare function run(): void;
