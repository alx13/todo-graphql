import yargs = require('yargs');
export { yargs, };
export interface Environment {
    [key: string]: string;
}
export declare function readEnvFile(cwd: string, file?: string): string;
export declare function checkEnv(env: Environment): Environment;
export declare function parseEnv(content: string, root?: string): Environment;
export declare function filterEnv(env: Environment, force?: boolean): Environment;
export declare function updateEnv(env: Environment): void;
export declare function loadEnv(file?: string, root?: string, override?: boolean): Environment;
export declare function loadAndUpdateEnv(files?: string[], root?: string, override?: boolean): void;
export declare function exec(cmdAndArgs: string[]): void;
