// tslint:disable:no-any

declare module '*/package.json' {
  const value: {
    version: string;
    [key: string]: any;
  };
  export = value;
}
