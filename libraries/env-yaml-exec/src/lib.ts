import * as fs from 'fs';
import * as path from 'path';

import yaml = require('js-yaml');
import yargs = require('yargs');
import kexec = require('kexec');

export {
  yargs,
};

export interface Environment {
  [key: string]: string;
}

export function readEnvFile(cwd: string, file?: string): string {
  try {
    return fs.readFileSync(
      path.resolve(cwd, (file || 'env.yaml')),
      'utf-8'
    );
  } catch (e) {
    if (e.code === 'ENOENT' && !file) {
      return '';
    }
    throw e;
  }
}

export function checkEnv(env: Environment): Environment {
  if (Array.isArray(env)) {
    throw new Error('env file should be an object');
  }
  Object.keys(env).forEach((key) => {
    // tslint:disable-next-line:strict-type-predicates
    if (typeof env[key] !== 'string') {
      throw new Error('env file should contain only strings');
    }
  });

  return env;
}

export function parseEnv(content: string, root?: string): Environment {
  const env = yaml.safeLoad(content) as { [key: string]: string | { [key: string]: string } };
  if (!env) {
    throw new Error(`Can't load content to yaml ${content}`);
  }

  if (root) {
    return env[root] as Environment;
  }

  return env as Environment;
}

export function filterEnv(env: Environment, force: boolean = false): Environment {
  checkEnv(env);

  // tslint:disable-next-line:no-inferred-empty-object-type
  return Object.keys(env)
    .reduce(
      (acc, key) => {
        if (!force && process.env[key]) {
          return {
            ...acc,
            [key]: process.env[key],
          };
        }

        return {
          ...acc,
          [key]: env[key],
        };
      },
      {}
    );
}

export function updateEnv(env: Environment): void {
  Object.keys(env).forEach((key) => {
    process.env[key] = env[key];
  });
}


export function loadEnv(file?: string, root?: string, override?: boolean): Environment {
  const content = readEnvFile(process.cwd(), file);
  const source = parseEnv(content, root);

  return filterEnv(source, override);
}

export function loadAndUpdateEnv(files?: string[], root?: string, override?: boolean): void {
  if (!files) {
    const env = loadEnv(undefined, root, override);
    updateEnv(env);

    return;
  }

  files.forEach((file) => {
    const env = loadEnv(file, root, override);
    updateEnv(env);
  });
}

export function exec(cmdAndArgs: string[]): void {
  const [cmd, ...args]: string[] = cmdAndArgs;
  kexec(cmd, args);
}

