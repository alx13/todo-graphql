#!/usr/bin/env node

import * as yargs from 'yargs';
import * as fs from 'fs' ;
import * as path from 'path';

const pkg = JSON.parse(
  fs.readFileSync(
    path.join(__dirname, '../package.json'),
    'utf-8'
  )
);

import {
  exec,
  loadAndUpdateEnv,
} from './lib';

export interface CLIOptions {
  _: string[];
  env?: string[];
  root?: string;
  force: boolean;
}

export function parseArgs(args: string[]): CLIOptions {
  return yargs
  .usage('Read .env and exec process\nUsage: $0 [options] <cmd> <args>')
  .version((pkg).version)
  .options({
    env: {
      alias: 'E',
      demandOption: false,
      describe: 'Location of .env file relative to current working directory',
      type: 'string',
    },
    force: {
      alias: 'f',
      default: false,
      demandOption: false,
      describe: 'Override existing env variables from .env file',
      type: 'boolean',
    },
    root: {
      alias: 'r',
      default: undefined,
      demandOption: false,
      describe: 'root key in yaml',
      type: 'string',
    },
  })
  .demandCommand(1)
  // tslint:disable-next-line:no-any
  .parse(args) as any as CLIOptions;
}


export function run(): void {
  const args = parseArgs(process.argv.slice(2));
  if (Array.isArray(args.env)) {
    loadAndUpdateEnv(args.env, args.root, args.force);
  } else if (args.env) {
    loadAndUpdateEnv([args.env], args.root, args.force);
  } else {
    loadAndUpdateEnv(undefined, args.root, args.force);
  }
  exec(args._);
}

if (require.main === module) {
  run();
}

