import { assert } from 'chai';

import {
  parseArgs,
} from '../src/app';

describe('app.ts', () => {
  describe('parseArgs', () => {
    const tr = parseArgs;
    it('should parse args', () => {
      assert.deepOwnInclude(
        tr([
          '-E', 'xxx.yaml',
          '-f',
          '--',
          'ls', '-la',
          '-h',
          '-f',
          '.',
        ]),
        {
          _: ['ls', '-la', '-h', '-f', '.'],
          env: 'xxx.yaml',
          force: true,
        }
      );
    });
    it('should parse args with multiple env', () => {
      assert.deepOwnInclude(
        tr([
          '-E', 'xxx.yaml',
          '-E', 'yyy.yaml',
          '-f',
          '--',
          'ls', '-la',
          '-h',
          '-f',
          '.',
        ]),
        {
          _: ['ls', '-la', '-h', '-f', '.'],
          env: ['xxx.yaml', 'yyy.yaml'],
          force: true,
        }
      );
    });
  });
});

