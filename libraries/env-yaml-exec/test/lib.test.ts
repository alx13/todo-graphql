import * as fs from 'fs';
import * as path from 'path';

import { assert } from 'chai';

import {
  checkEnv,
  filterEnv,
  parseEnv,
  readEnvFile,
  updateEnv,
  loadAndUpdateEnv,
} from '../src/lib';

describe('lib.ts', () => {
  const fixtures = path.join(__dirname, 'fixtures');

  describe('readEnvFile', () => {
    const tr = readEnvFile;
    it('should return content for the real file', () => {
      assert.deepEqual(
        tr(fixtures, 'plain.yaml'),
        fs.readFileSync(path.join(fixtures, 'plain.yaml'), 'utf-8')
      );
    });
    it('should read default env.yaml file', () => {
      assert.deepEqual(
        tr(fixtures),
        fs.readFileSync(path.join(fixtures, 'env.yaml'), 'utf-8')
      );
    });
    it('should return empty string for missing default env.yaml', () => {
      assert.deepEqual(
        tr(process.cwd()),
        ''
      );
    });
    it('should throw error for missing implicit file', () => {
      assert.throws(() => tr(fixtures, 'missing.yaml'));
    });
  });
  describe('checkEnv', () => {
    const tr = checkEnv;
    it('should pass empty object', () => {
      assert.deepEqual(tr({}), {});
    });
    it('should throw error for array', () => {
      // @ts-ignore
      assert.throws(() => tr([]));
    });
    it('should throw error for non-string values', () => {
      // @ts-ignore
      assert.throws(() => tr({ a: [] }));
      // @ts-ignore
      assert.throws(() => tr({ a: {} }));
      // @ts-ignore
      assert.throws(() => tr({ a: 1 }));
    });
  });

  describe('parseEnv', () => {
    const tr = parseEnv;
    it('should parse from root', () => {
      assert.deepEqual(
        tr(fs.readFileSync(path.join(fixtures, 'plain.yaml'), 'utf-8')),
        {
          A: '1',
          B: '{"x": 1, "y": 2}',
          C: '',
        }
      );
    });
    it('should parse from subkey', () => {
      assert.deepEqual(
        tr(
          fs.readFileSync(path.join(fixtures, 'nested.yaml'), 'utf-8'),
          'env'
        ),
        {
          A: '1',
        }
      );
    });
  });

  describe('filterEnv', () => {
    const tr = filterEnv;
    let savedEnv = {};
    beforeEach(() => {
      savedEnv = {...process.env};
    });
    afterEach(() => {
      process.env = {};
      Object.assign(process.env, savedEnv);
    });
    it('without force', () => {
      process.env.dotenv_exec_defined_empty = '';
      process.env.dotenv_exec_defined_full = 'defined';

      assert.deepEqual(
        tr({
          dotenv_exec_defined_empty: '',
          dotenv_exec_defined_full: 'redefined',
          dotenv_exec_undefined_empty: '',
          dotenv_exec_undefined_full: 'undefined',
        }),
        {
          dotenv_exec_defined_empty: '',
          dotenv_exec_defined_full: 'defined',
          dotenv_exec_undefined_empty: '',
          dotenv_exec_undefined_full: 'undefined',
        }
      );
    });
    it('with force', () => {
      process.env.dotenv_exec_defined_empty = '';
      process.env.dotenv_exec_defined_full = 'defined';

      assert.deepEqual(
        tr(
          {
            dotenv_exec_defined_empty: '',
            dotenv_exec_defined_full: 'redefined',
            dotenv_exec_undefined_empty: '',
            dotenv_exec_undefined_full: 'undefined',
          },
          true
        ),
        {
          dotenv_exec_defined_empty: '',
          dotenv_exec_defined_full: 'redefined',
          dotenv_exec_undefined_empty: '',
          dotenv_exec_undefined_full: 'undefined',
        }
      );
    });
    it('should check env', () => {
      // @ts-ignore
      assert.throws(() => tr({ a: 1 }));
    });
  });

  describe('updateEnv', () => {
    const tr = updateEnv;
    let savedEnv = {};
    beforeEach(() => {
      savedEnv = {...process.env};
    });
    afterEach(() => {
      process.env = {};
      Object.assign(process.env, savedEnv);
    });
    it('should update process.env', () => {
      const src = {
        dotenv_exec_defined_empty: '',
        dotenv_exec_defined_full: 'defined',
        dotenv_exec_undefined_empty: '',
        dotenv_exec_undefined_full: 'undefined',
      };
      assert.doesNotHaveAnyKeys(process.env, src);
      tr(src);
      assert.ownInclude(
        process.env,
        {
          dotenv_exec_defined_empty: '',
          dotenv_exec_defined_full: 'defined',
          dotenv_exec_undefined_empty: '',
          dotenv_exec_undefined_full: 'undefined',
        }
      );
    });
  });

  describe('loadEnv', () => {
    let savedEnv = {};
    beforeEach(() => {
      savedEnv = {...process.env};
    });
    afterEach(() => {
      process.env = {};
      Object.assign(process.env, savedEnv);
    });
    it('should load file', () => {
      loadAndUpdateEnv([path.join(fixtures, 'env.yaml')], undefined, true);
      assert.ownInclude(
        process.env,
        {
          A: '2',
        }
      );
    });
    it('should load multiple files', () => {
      loadAndUpdateEnv(
        [
          path.join(fixtures, 'env.yaml'),
          path.join(fixtures, 'plain.yaml'),
        ],
        undefined,
        false
      );
      assert.ownInclude(
        process.env,
        {
          A: '2',
          B: '{"x": 1, "y": 2}',
          C: '',
        }
      );
    });
    it('should load multiple files with override', () => {
      loadAndUpdateEnv(
        [
          path.join(fixtures, 'env.yaml'),
          path.join(fixtures, 'plain.yaml'),
        ],
        undefined,
        true
      );
      assert.ownInclude(
        process.env,
        {
          A: '1',
          B: '{"x": 1, "y": 2}',
          C: '',
        }
      );
    });
    it('should load nested file', () => {
      loadAndUpdateEnv([path.join(fixtures, 'nested.yaml')], 'env', true);
      assert.ownInclude(
        process.env,
        {
          A: '1',
        }
      );
    });
  });
});

