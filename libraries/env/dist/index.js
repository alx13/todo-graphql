"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const os_1 = tslib_1.__importDefault(require("os"));
const path_1 = tslib_1.__importDefault(require("path"));
const assert_1 = tslib_1.__importDefault(require("assert"));
function getAr(env) {
    return (key) => {
        assert_1.default.ok(env[key], `${key} env var isn't set`);
        return env[key];
    };
}
function parseEnv(env) {
    const ar = getAr(env);
    const serviceName = ar('TDG__SERVICE_NAME');
    const serviceVersion = ar('TDG__SERVICE_VERSION');
    const logLevel = env['TDG__LOG_LEVEL'] || 'info';
    return {
        serviceName,
        serviceVersion,
        logLevel,
        tmpdir: env['TDG__TMPDIR'] || path_1.default.join(os_1.default.tmpdir(), serviceName, serviceVersion),
        keepFiles: env['TDG__KEEP_FILES'] === 'true',
        features: {},
        backend: {
            graphqlEndpoint: ar('TDG__BACKEND_GRAPHQL_ENDPOINT'),
            internal: {
                useHttp: env['TDG__BACKEND_INTERNAL_USE_HTTP'] === 'true',
                host: ar('TDG__BACKEND_INTERNAL_HOST'),
                port: parseInt(ar('TDG__BACKEND_INTERNAL_PORT'), 10),
            },
            external: {
                useHttp: env['TDG__BACKEND_EXTERNAL_USE_HTTP'] === 'true',
                host: ar('TDG__BACKEND_EXTERNAL_HOST'),
                port: parseInt(ar('TDG__BACKEND_EXTERNAL_PORT'), 10),
            },
        },
        frontend: {
            external: {
                useHttp: env['TDG__FRONTEND_EXTERNAL_USE_HTTP'] === 'true',
                host: ar('TDG__FRONTEND_EXTERNAL_HOST'),
                port: parseInt(ar('TDG__FRONTEND_EXTERNAL_PORT'), 10),
            },
            internal: {
                port: parseInt(ar('TDG__FRONTEND_INTERNAL_PORT'), 10),
            },
        },
        postgres: {
            user: ar('TDG__POSTGRES_USER'),
            password: ar('TDG__POSTGRES_PASSWORD'),
            database: ar('TDG__POSTGRES_DATABASE'),
            host: ar('TDG__POSTGRES_HOST'),
            port: parseInt(env['TDG__POSTGRES_PORT'] || '5432', 10),
            ssl: env['TDG__POSTGRES_SSL'] !== 'false',
            minPoolSize: parseInt(env['TDG__POSTGRES_MIN_POOL_SIZE'] || '1', 0),
            maxPoolSize: parseInt(env['TDG__POSTGRES_MAX_POOL_SIZE'] || '1', 0),
        },
    };
}
exports.parseEnv = parseEnv;
//# sourceMappingURL=index.js.map