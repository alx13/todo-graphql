/// <reference types="node" />
export declare function parseEnv(env: NodeJS.ProcessEnv): {
    serviceName: string;
    serviceVersion: string;
    logLevel: string;
    tmpdir: string;
    keepFiles: boolean;
    features: {};
    backend: {
        graphqlEndpoint: string;
        internal: {
            useHttp: boolean;
            host: string;
            port: number;
        };
        external: {
            useHttp: boolean;
            host: string;
            port: number;
        };
    };
    frontend: {
        external: {
            useHttp: boolean;
            host: string;
            port: number;
        };
        internal: {
            port: number;
        };
    };
    postgres: {
        user: string;
        password: string;
        database: string;
        host: string;
        port: number;
        ssl: boolean;
        minPoolSize: number;
        maxPoolSize: number;
    };
};
export declare type Env = ReturnType<typeof parseEnv>;
