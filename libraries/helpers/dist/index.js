"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function makeSiteUrl({ useHttp, host, port, websockets, route = '', }) {
    const defaultPort = useHttp ? 80 : 443;
    return [
        websockets ? 'ws' : 'http',
        useHttp ? '' : 's',
        '://',
        host,
        port === defaultPort ? '' : `:${port}`,
        route,
    ].join('');
}
exports.makeSiteUrl = makeSiteUrl;
//# sourceMappingURL=index.js.map