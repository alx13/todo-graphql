export interface MakeSiteUrlParams {
    useHttp: boolean;
    host: string;
    port: number;
    websockets?: boolean;
    route?: string;
}
export declare function makeSiteUrl({ useHttp, host, port, websockets, route, }: MakeSiteUrlParams): string;
