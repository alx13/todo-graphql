all: build

build-backend:
	$(MAKE) -C ./apps/backend build
build-frontend:
	$(MAKE) -C ./apps/backend build

build: build-frontend
build: build-backend

clean-node_modules:
	pnpm recursive exec rm -- -Rf node_modules

bootstrap:
	pnpm recursive install

reinit: clean-node_modules
reinit: bootstrap

start-backend-dev:
	${MAKE} -C ./apps/backend start-dev

start-frontend-dev:
	${MAKE} -C ./apps/frontend start-dev

start-gql-codegen:
	$(MAKE) -C ./tools/api-gen watch

start-postgres:
	cd ./services/postgres && docker-compose up

test-backend-spec:
	$(MAKE) -C ./apps/backend test-spec
test-backend-e2e:
	$(MAKE) -C ./apps/backend test-e2e

test-backend: test-backend-spec
test-backend: test-backend-e2e

test-libraries-spec:
	$(MAKE) -C ./libraries/env-yaml-exec test-spec

test-spec: test-backend-spec
test-spec: test-libraries-spec

clean-postgres:
	rm -Rf ./services/postgres/data/postgres

check-deps: NPM_CHECK_INSTALLER=pnpm
check-deps:
	pnpm recursive exec --no-bail npm-check -- \
		-s \
		--ignore '@types/node'

docker-compose-up:
	docker-compose \
		-f ./services/postgres/docker-compose.yml \
		-f ./services/app/docker-compose.yml \
		up

docker-compose-build:
	docker-compose \
		-f ./services/postgres/docker-compose.yml \
		-f ./services/app/docker-compose.yml \
		build

