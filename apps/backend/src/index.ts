import http from 'http';
import express from 'express';
import cors from 'cors';
import { ApolloServer } from 'apollo-server-express';
import Knex from 'knex';
import { knexSnakeCaseMappers } from 'objection';

import { parseEnv, Env } from '@todo-graphql/env';

import { AppModule } from './modules';
import { makeSiteUrl } from './helpers';

export async function initApolloServer(env: Env) {
  const isProduction = process.env.NODE_ENV === 'production';
  const knex = Knex({
    pool: {
      min: env.postgres.minPoolSize,
      max: env.postgres.maxPoolSize,
    },
    connection: {
      ...env.postgres,
    },
    client: 'pg',
    debug: false,
    ...knexSnakeCaseMappers(),
  });

  const {
    schema,
    context,
    subscriptions,
  } = AppModule.forRoot({ knex });

  return {
    knex,
    schema,
    context,
    subscriptions,
    apolloServer: new ApolloServer({
      context,
      schema,
      subscriptions: {
        ...subscriptions,
        path: env.backend.graphqlEndpoint,
      },
      debug: true,
      introspection: !isProduction,
      tracing: !isProduction,
      playground: !isProduction,
    })
  };
}

export async function initServer() {
  const env = parseEnv(process.env);

  const app = express();

  const whitelistOrigins = [
    makeSiteUrl(env.backend.external),
    makeSiteUrl(env.frontend.external),
  ].filter((origin): origin is string => Boolean(origin));

  const corsMiddleware = cors({
    origin: whitelistOrigins,
    credentials: true,
  });

  app.use(corsMiddleware);

  const httpServer = http.createServer(app);

  const { apolloServer } = await initApolloServer(env);
  apolloServer.applyMiddleware({
    app,
    path: env.backend.graphqlEndpoint,
    cors: false,
  });
  apolloServer.installSubscriptionHandlers(httpServer);

  httpServer.listen(
    { port: env.backend.internal.port },
    () => {
      console.log([
        '',
        '🚀 Server ready at:',
        `Internal: ${makeSiteUrl(env.backend.internal)}`,
        `External: ${makeSiteUrl(env.backend.external)}`,
        '',
        'Graphql:',
        `Internal: ${makeSiteUrl(env.backend.internal)}${apolloServer.graphqlPath}`,
        `External: ${makeSiteUrl(env.backend.external)}${apolloServer.graphqlPath}`,
        '',
      ].join('\n'));
    }
  );
}

if (require.main === module) {
  initServer()
    .catch((err) => { console.error(err); });
}
