export interface MakeSiteUrlParams {
  useHttp: boolean;
  host: string;
  port: number;
  websockets?: boolean;
  route?: string;
}
export function makeSiteUrl({
  useHttp,
  host,
  port,
  websockets,
  route = '',
}: MakeSiteUrlParams) {
  const defaultPort = useHttp ? 80 : 443;

  return [
    websockets ? 'ws' : 'http',
    useHttp ? '' : 's',
    '://',
    host,
    port === defaultPort ? '' : `:${port}`,
    route,
  ].join('');
}
