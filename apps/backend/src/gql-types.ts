// tslint:disable
import { GraphQLResolveInfo } from "graphql";
import { ModuleContext } from "@graphql-modules/core";
export type Maybe<T> = T | null | undefined;
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type GQLMutation = {
  __typename?: "Mutation";
  createTask: GQLTask;
  updateTask: GQLTask;
  changeTaskState: GQLTask;
};

export type GQLMutationCreateTaskArgs = {
  input: GQLTaskInput;
};

export type GQLMutationUpdateTaskArgs = {
  id: Scalars["ID"];
  input: GQLTaskInput;
};

export type GQLMutationChangeTaskStateArgs = {
  id: Scalars["ID"];
  state: GQLTaskState;
};

export type GQLQuery = {
  __typename?: "Query";
  tasks: Array<GQLTask>;
  taskById?: Maybe<GQLTask>;
};

export type GQLQueryTaskByIdArgs = {
  id: Scalars["ID"];
};

export type GQLTask = {
  __typename?: "Task";
  id: Scalars["ID"];
  title: Scalars["String"];
  description?: Maybe<Scalars["String"]>;
  state: GQLTaskState;
  canGo: Array<GQLTaskState>;
};

export type GQLTaskInput = {
  /**  required for createTask  */
  title?: Maybe<Scalars["String"]>;
  description?: Maybe<Scalars["String"]>;
  state?: Maybe<GQLTaskState>;
};

export enum GQLTaskState {
  open = "open",
  pending = "pending",
  closed = "closed"
}
export type WithIndex<TObject> = TObject & Record<string, any>;
export type ResolversObject<TObject> = WithIndex<TObject>;

export type ResolverTypeWrapper<T> = Promise<T> | T;

export type ResolverFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => Promise<TResult> | TResult;

export type StitchingResolver<TResult, TParent, TContext, TArgs> = {
  fragment: string;
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};

export type Resolver<TResult, TParent = {}, TContext = {}, TArgs = {}> =
  | ResolverFn<TResult, TParent, TContext, TArgs>
  | StitchingResolver<TResult, TParent, TContext, TArgs>;

export type SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => AsyncIterator<TResult> | Promise<AsyncIterator<TResult>>;

export type SubscriptionResolveFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

export interface SubscriptionResolverObject<TResult, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs>;
  resolve?: SubscriptionResolveFn<TResult, TParent, TContext, TArgs>;
}

export type SubscriptionResolver<
  TResult,
  TParent = {},
  TContext = {},
  TArgs = {}
> =
  | ((
      ...args: any[]
    ) => SubscriptionResolverObject<TResult, TParent, TContext, TArgs>)
  | SubscriptionResolverObject<TResult, TParent, TContext, TArgs>;

export type TypeResolveFn<TTypes, TParent = {}, TContext = {}> = (
  parent: TParent,
  context: TContext,
  info: GraphQLResolveInfo
) => Maybe<TTypes>;

export type NextResolverFn<T> = () => Promise<T>;

export type DirectiveResolverFn<
  TResult = {},
  TParent = {},
  TContext = {},
  TArgs = {}
> = (
  next: NextResolverFn<TResult>,
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

/** Mapping between all available schema types and the resolvers types */
export type GQLResolversTypes = ResolversObject<{
  Query: ResolverTypeWrapper<{}>;
  Task: ResolverTypeWrapper<Partial<GQLTask>>;
  ID: ResolverTypeWrapper<Partial<Scalars["ID"]>>;
  String: ResolverTypeWrapper<Partial<Scalars["String"]>>;
  TaskState: ResolverTypeWrapper<Partial<GQLTaskState>>;
  Mutation: ResolverTypeWrapper<{}>;
  TaskInput: ResolverTypeWrapper<Partial<GQLTaskInput>>;
  Boolean: ResolverTypeWrapper<Partial<Scalars["Boolean"]>>;
}>;

/** Mapping between all available schema types and the resolvers parents */
export type GQLResolversParentTypes = ResolversObject<{
  Query: {};
  Task: Partial<GQLTask>;
  ID: Partial<Scalars["ID"]>;
  String: Partial<Scalars["String"]>;
  TaskState: Partial<GQLTaskState>;
  Mutation: {};
  TaskInput: Partial<GQLTaskInput>;
  Boolean: Partial<Scalars["Boolean"]>;
}>;

export type GQLMutationResolvers<
  ContextType = ModuleContext,
  ParentType = GQLResolversParentTypes["Mutation"]
> = ResolversObject<{
  createTask?: Resolver<
    GQLResolversTypes["Task"],
    ParentType,
    ContextType,
    GQLMutationCreateTaskArgs
  >;
  updateTask?: Resolver<
    GQLResolversTypes["Task"],
    ParentType,
    ContextType,
    GQLMutationUpdateTaskArgs
  >;
  changeTaskState?: Resolver<
    GQLResolversTypes["Task"],
    ParentType,
    ContextType,
    GQLMutationChangeTaskStateArgs
  >;
}>;

export type GQLQueryResolvers<
  ContextType = ModuleContext,
  ParentType = GQLResolversParentTypes["Query"]
> = ResolversObject<{
  tasks?: Resolver<Array<GQLResolversTypes["Task"]>, ParentType, ContextType>;
  taskById?: Resolver<
    Maybe<GQLResolversTypes["Task"]>,
    ParentType,
    ContextType,
    GQLQueryTaskByIdArgs
  >;
}>;

export type GQLTaskResolvers<
  ContextType = ModuleContext,
  ParentType = GQLResolversParentTypes["Task"]
> = ResolversObject<{
  id?: Resolver<GQLResolversTypes["ID"], ParentType, ContextType>;
  title?: Resolver<GQLResolversTypes["String"], ParentType, ContextType>;
  description?: Resolver<
    Maybe<GQLResolversTypes["String"]>,
    ParentType,
    ContextType
  >;
  state?: Resolver<GQLResolversTypes["TaskState"], ParentType, ContextType>;
  canGo?: Resolver<
    Array<GQLResolversTypes["TaskState"]>,
    ParentType,
    ContextType
  >;
}>;

export type GQLResolvers<ContextType = ModuleContext> = ResolversObject<{
  Mutation?: GQLMutationResolvers<ContextType>;
  Query?: GQLQueryResolvers<ContextType>;
  Task?: GQLTaskResolvers<ContextType>;
}>;
