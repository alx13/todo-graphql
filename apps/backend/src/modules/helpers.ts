import fs from 'fs';
import path from 'path';

import _ from 'lodash';
import glob from 'glob';
import {
  parse,
  visit,
  extendSchema,
} from 'graphql';

export type SchemaScope =
  | '*';

export interface LoadTypeDefsOptions {
  cwd: string;
  scopes: SchemaScope[];
}

export function loadTypeDefs({
  cwd,
  scopes,
}: LoadTypeDefsOptions) {
  if (scopes.length === 0) {
    throw new Error('Empty scopes');
  }

  return _.chain(scopes)
    .map((scope) => `${scope}`)
    .join('|')
    .thru((patterns) => `**/@(${patterns})*.graphql`)
    .thru((pattern) => glob.sync(
      pattern,
      {
        cwd,
      }
    ))
    .map((file) => fs.readFileSync(path.join(cwd, file), 'utf-8'))
    .map((typeDef) => parse(typeDef))
    .value();
}

import { GraphQLModule } from '@graphql-modules/core';

export function directiveFixingMiddleware({ schema, typeDefs }: GraphQLModule) {
  if (!typeDefs) {
    return { schema };
  }

  const directiveTypeDefs = visit(
    typeDefs,
    {
      enter: (node) => {
        if (node.kind === 'Document') {
          return undefined;
        }
        if (node.kind === 'DirectiveDefinition') {
          const foundDirective = schema.getDirective(node.name.value);

          return foundDirective
            ? null
            : false;
        }

        return null;
      },
    }
  );

  if (directiveTypeDefs.definitions && directiveTypeDefs.definitions.length > 0) {
    const fixedSchema = extendSchema(schema, directiveTypeDefs);

    return { schema: fixedSchema };
  }

  return { schema };
}
