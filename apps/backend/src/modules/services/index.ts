import Knex from 'knex';
import { GraphQLModule } from '@graphql-modules/core';

import { Postgreser } from '#/postgreser';

import { directiveFixingMiddleware } from '#/modules/helpers';

export interface ServicesModuleConfig {
  knex: Knex;
}

export const ServicesModule = new GraphQLModule<ServicesModuleConfig>({
  name: 'Services',
  providers({ config: { knex } }) {
    return [
      { provide: Postgreser, useValue: new Postgreser(knex) },
    ];
  },
  configRequired: true,
  middleware: directiveFixingMiddleware,
});
