import { GraphQLModule } from '@graphql-modules/core';

import { directiveFixingMiddleware } from '#/modules/helpers';
import { TaskModule } from '#/modules/task';

import {
  ServicesModule,
  ServicesModuleConfig,
} from './services';

type AppModuleConfig = ServicesModuleConfig;

export const AppModule = new GraphQLModule<AppModuleConfig>({
  imports({ config }) {
    return [
      ServicesModule.forRoot(config),
      TaskModule,
    ];
  },
  middleware: directiveFixingMiddleware,
});
