import { Injectable } from '@graphql-modules/di';
import {
  Postgreser,
  Transaction,
  Task,
  QueryBuilderYieldingOneOrNone,
} from '#/postgreser';

@Injectable()
export class TaskProvider {
  public constructor(
    private readonly postgreser: Postgreser
  ) {}

  public async getTasks(
    trx: Transaction | undefined
  ) {
    return this.postgreser.Task.query(trx);
  }

  public async getTaskById(
    trx: Transaction | undefined,
    taskId: Task['id'],
    modify?: (builder: QueryBuilderYieldingOneOrNone<Task>) => void
  ) {
    const query = this.postgreser.Task
      .query(trx)
      .findById(taskId);

    if (modify) {
      modify(query);
    }

    return query;
  }

  public async createTask(
    trx: Transaction | undefined,
    taskInput: Partial<Task>,
    modify?: (builder: QueryBuilderYieldingOneOrNone<Task>) => void
  ) {
    const query = this.postgreser.Task
      .query(trx)
      .insertAndFetch(taskInput);

    if (modify) {
      modify(query);
    }

    return query;
  }

  public checkNewTaskState(task: Task, newState: Task['state'] | undefined) {
    if (!newState) {
      return;
    }

    if (newState === task.state) {
      return;
    }

    if (task.canGo.includes(newState)) {
      return;
    }

    throw new Error(`Can\'t change task "${task.id}" state from "${task.state}" to "${newState}"`);
  }

  public async updateTask(
    trx: Transaction | undefined,
    taskId: Task['id'],
    taskInput: Partial<Task>,
    modify?: (builder: QueryBuilderYieldingOneOrNone<Task>) => void
  ) {
    const task = await this.postgreser.Task
      .query(trx)
      .findById(taskId)
      .throwIfNotFound();

    this.checkNewTaskState(task, taskInput.state);

    const query = task.$query(trx)
      .patchAndFetch(
        taskInput
      );

    if (modify) {
      modify(query);
    }

    return query;
  }

  public async changeTaskState(
    trx: Transaction | undefined,
    taskId: Task['id'],
    newTaskState: Task['state'],
    modify?: (builder: QueryBuilderYieldingOneOrNone<Task>) => void
  ) {
    const task = await this.getTaskById(
      trx,
      taskId,
      (builder) => { builder.throwIfNotFound(); }
    ) as Task;

    if (task.state === newTaskState) {
      const fetchQuery = task.$query(trx);
      if (modify) {
        modify(fetchQuery);

        return fetchQuery;
      }
    }

    this.checkNewTaskState(task, newTaskState);

    const query = this.postgreser.Task
      .query(trx)
      .patchAndFetchById(
        taskId,
        {
          state: newTaskState,
        }
      );

    if (modify) {
      modify(query);
    }

    return query;
  }
}
