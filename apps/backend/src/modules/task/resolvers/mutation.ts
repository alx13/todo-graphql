import { GQLMutationResolvers } from '#/gql-types';
import { Postgreser, Task } from '#/postgreser';
import { TaskProvider } from '../provider';

export const Mutation: GQLMutationResolvers = {
  async createTask(_root, { input }, { injector }, _info) {
    const postgreser = injector.get(Postgreser);
    const taskProvider = injector.get(TaskProvider);

    // we have to validate input here
    if (!input.title) {
      throw new Error('Title can\'t be null');
    }

    return postgreser
      .transaction(async (trx) => taskProvider.createTask(trx, input as Partial<Task>));
  },
  async updateTask(_root, { id, input }, { injector }, _info) {
    const postgreser = injector.get(Postgreser);
    const taskProvider = injector.get(TaskProvider);

    if (input.title === null) {
      throw new Error('Title can\'t be null');
    }

    if (input.state === null) {
      throw new Error('State can\'t be null');
    }

    return postgreser
      .transaction(async (trx) => taskProvider.updateTask(trx, id, input as Partial<Task>));
  },
  async changeTaskState(_root, { id, state }, { injector }, _info) {
    const postgreser = injector.get(Postgreser);
    const taskProvider = injector.get(TaskProvider);

    return postgreser
      .transaction(async (trx) => taskProvider.changeTaskState(trx, id, state));
  },
};

