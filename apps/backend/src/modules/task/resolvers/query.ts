import { GQLQueryResolvers } from '#/gql-types';
import { TaskProvider } from '../provider';
import { Postgreser } from '#/postgreser';

export const Query: GQLQueryResolvers = {
  async tasks(_root, _args, { injector }, _info) {
    const postgreser = injector.get(Postgreser);
    const taskProvider = injector.get(TaskProvider);

    return postgreser
      .transaction(async (trx) => taskProvider.getTasks(trx));
  },

  async taskById(_root, { id }, { injector }, _info) {
    const postgreser = injector.get(Postgreser);
    const taskProvider = injector.get(TaskProvider);

    return postgreser
      .transaction(async (trx) => taskProvider.getTaskById(trx, id));
  },
};
