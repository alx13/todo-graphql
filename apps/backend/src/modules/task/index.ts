import { GraphQLModule } from '@graphql-modules/core';

import { loadTypeDefs } from '#/modules/helpers';

import { resolvers } from './resolvers';
import { TaskProvider } from './provider';
import { ServicesModule } from '../services';

export {
  TaskProvider,
};

export const TaskModule = new GraphQLModule({
  resolvers,
  typeDefs: loadTypeDefs({
    cwd: __dirname,
    scopes: ['*'],
  }),
  imports: [
    ServicesModule,
  ],
  providers: [
    { provide: TaskProvider, useClass: TaskProvider },
  ],
});
