import 'reflect-metadata';
import _ from 'lodash';
import Knex from 'knex';
import {
  Transaction,
  Model,
  transaction,
  ModelClass,
  RelationExpression,
  Ids,
  Id,
  QueryBuilder,
  QueryBuilderYieldingOneOrNone,
  ref,
} from 'objection';

export {
  Id,
  Ids,
  Transaction,
  Model,
  ModelClass,
  RelationExpression,
  QueryBuilder,
  QueryBuilderYieldingOneOrNone,
  ref,
};

import { Task } from './task';

export * from './task';

export class Postgreser {
  public readonly knex: Knex;

  public readonly Task!: typeof Task;

  public constructor(knex: Knex) {
    this.knex = knex;

    this.Task = Task.bindKnex(this.knex);
  }

  public async destroy() {
    return this.knex.destroy();
  }

  public async transaction<V>(
    callback: (trx: Transaction) => Promise<V>
  ): Promise<V> {
    return transaction(this.knex, callback);
  }

  public async now(trx?: Transaction) {
    const builder = trx
     ? trx
     : this.knex;

    const [{ now: currentTimestamp }] = await builder
      .select(this.knex.fn.now());

    return currentTimestamp as Date;
  }
}
