import 'reflect-metadata';

import { Model, JsonSchema } from 'objection';

import {
  GQLTaskState,
  GQLTask,
} from '#/gql-types';

export {
  GQLTaskState as TaskState,
};

export class Task extends Model implements GQLTask {
  public static tableName = 'tasks';
  public static idColumn = 'id';

  public readonly id!: string;
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
  public title!: string;
  public description!: string | null;
  public state!: GQLTaskState;

  // If this grows consider using FSM
  public get canGo() {
    switch (this.state) {
      case GQLTaskState.open:
        return [GQLTaskState.pending, GQLTaskState.closed];
      case GQLTaskState.pending:
        return [GQLTaskState.closed];
      case GQLTaskState.closed:
        return [];
      default:
        throw new Error(`Unknown initial state "${this.state}"`);
    }
  }

  public static jsonSchema: JsonSchema = {
    type: 'object',
    required: ['title'],
    properties: {
      id: { type: 'string' },
      title: { type: 'string' },
      state: { type: 'string', enum: Object.values(GQLTaskState) },
    },
  };
}

