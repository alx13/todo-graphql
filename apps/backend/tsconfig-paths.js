const path = require('path');
const tsConfig = require('./tsconfig.json');

require('tsconfig-paths').register({
  baseUrl: path.join(__dirname, './dist/'),
  paths: tsConfig.compilerOptions.paths,
});
