import Knex from 'knex';
import {
  ApolloServer,
  gql,
} from 'apollo-server-express';
import {
  createTestClient,
  ApolloServerTestClient,
} from 'apollo-server-testing';
import { parseEnv } from '@todo-graphql/env';
import { assert } from 'chai';

import { initApolloServer } from '#/index';
import { TaskState, Task } from '#/postgreser';
import {
  GQLTask,
  GQLMutationUpdateTaskArgs,
  GQLMutationChangeTaskStateArgs,
} from '#/gql-types';

describe('TaskModule', () => {
  let apolloServer: ApolloServer;
  let knex: Knex;
  let client: ApolloServerTestClient;
  let TASKS: Partial<GQLTask>[];
  const INSERTED_TASKS: GQLTask[] = [];

  before(async () => {
    const env = parseEnv(process.env);
    const server = await initApolloServer(env);
    apolloServer = server.apolloServer;
    knex = server.knex;
    client = createTestClient(apolloServer);

    const ts = new Date().valueOf();

    TASKS = [
      { title: `task 1 (e2e test) ${ts}` },
      { title: `task 2 (e2e test) ${ts}`, state: TaskState.pending },
      { title: `task 3 (e2e test) ${ts}`, description: 'task 3 description (e2e test)' },
    ];
  });

  after(async () => {
    await knex.destroy();
  });

  const taskFragment = gql`
    fragment taskFragment on Task {
      id
      state
      title
      description
      canGo
    }
  `;

  it('Mutation.createTask ok', async () => {
    for (const task of TASKS) {
      const res = await client.mutate({
        mutation: gql`
          mutation createTask($input: TaskInput!) {
            task: createTask(input: $input) {
              ...taskFragment
            }
          }
          ${taskFragment}
        `,
        variables: { input: task },
      });

      assert.isObject(res);
      assert.isNotOk(res.errors);
      const { data } = res;
      assert.isObject(data);
      assert.deepOwnInclude((data as { task: GQLTask }).task, task);

      INSERTED_TASKS.push((data as { task: GQLTask }).task);
    }
  });
  it('Query.tasks ok', async () => {
    const res = await client.query({
      query: gql`
          query tasks {
            tasks: tasks {
              ...taskFragment
            }
          }
          ${taskFragment}
        `,
    });

    assert.isObject(res);
    assert.isNotOk(res.errors);
    const { data } = res;
    assert.isObject(data);
    assert.includeDeepMembers((data as { tasks: GQLTask[] }).tasks, INSERTED_TASKS);
  });
  it('Mutation.updateTask ok', async () => {
    const newState = TaskState.pending;
    const taskToUpdateIndex = INSERTED_TASKS
      .findIndex(({ canGo }) => canGo.includes(newState));
    const taskToUpdate = INSERTED_TASKS[taskToUpdateIndex];
    const uts = new Date().valueOf();

    const input: GQLMutationUpdateTaskArgs['input'] = {
      title: `${taskToUpdate.title || ''} updated ${uts}`,
      description: `${taskToUpdate.description || ''} updated ${uts}`,
      state: newState,
    };

    const res = await client.mutate({
      mutation: gql`
        mutation updateTask($id: ID!, $input: TaskInput!) {
          task: updateTask(id: $id, input: $input) {
            ...taskFragment
          }
        }
        ${taskFragment}
      `,
      variables: {
        input,
        id: taskToUpdate.id,
      },
    });

    assert.isObject(res);
    assert.isNotOk(res.errors);
    const { data } = res;
    assert.isObject(data);
    const fetchedTask = (data as { task: GQLTask }).task;
    assert.deepEqual(
      fetchedTask,
      {
        ...taskToUpdate,
        ...input,
        canGo: Task.fromJson({ ...input }).canGo,
      }
    );
    INSERTED_TASKS[taskToUpdateIndex] = fetchedTask;
  });
  it('Mutation.changeTaskState ok', async () => {
    const taskToUpdateIndex = INSERTED_TASKS
      .findIndex(({ canGo }) => canGo.length > 0);
    const taskToUpdate = INSERTED_TASKS[taskToUpdateIndex];

    const newState: GQLMutationChangeTaskStateArgs['state'] = taskToUpdate.canGo[0];

    const res = await client.mutate({
      mutation: gql`
        mutation changeTaskState($id: ID!, $state: TaskState!) {
          task: changeTaskState(id: $id, state: $state) {
            ...taskFragment
          }
        }
        ${taskFragment}
      `,
      variables: {
        id: taskToUpdate.id,
        state: newState,
      },
    });

    assert.isObject(res);
    assert.isNotOk(res.errors);
    const { data } = res;
    assert.isObject(data);
    const fetchedTask = (data as { task: GQLTask }).task;
    assert.deepEqual(
      fetchedTask,
      {
        ...taskToUpdate,
        state: newState,
        canGo: Task.fromJson({
          title: taskToUpdate.title,
          state: newState,
        }).canGo,
      }
    );
    INSERTED_TASKS[taskToUpdateIndex] = fetchedTask;
  });
});
