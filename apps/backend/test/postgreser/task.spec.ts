import { assert } from 'chai';
import { Task, TaskState } from '#/postgreser';

describe('postgreser.Task', async () => {
  it('canGo', async () => {
    const stub = {
      id: '1',
      title: '1',
    };

    assert.sameMembers(
      Task.fromJson({ ...stub, state: TaskState.open }).canGo,
      [TaskState.pending, TaskState.closed]
    );
    assert.sameMembers(
      Task.fromJson({ ...stub, state: TaskState.pending }).canGo,
      [TaskState.closed]
    );
    assert.sameMembers(
      Task.fromJson({ ...stub, state: TaskState.closed }).canGo,
      []
    );
  });
});
