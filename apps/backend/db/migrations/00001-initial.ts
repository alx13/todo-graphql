import * as Knex from 'knex';
import { GQLTaskState } from '#/gql-types';

import {
  timestamps,
  createUpdatedAtTrigger,
  dropUpdatedAtTrigger,
} from '../helpers';

export async function up(knex: Knex): Promise<void> {
  await knex.raw('CREATE EXTENSION IF NOT EXISTS "pgcrypto"');
  await knex.schema.createTable('tasks', (table) => {
    // I was lazy here, its better to use http://en.wikipedia.org/wiki/Feistel_cipher
    // https://wiki.postgresql.org/wiki/Pseudo_encrypt
    table.uuid('id').primary().defaultTo(knex.raw('gen_random_uuid()'));
    timestamps(knex, table);
    table.text('title').notNullable();
    table.text('description');
    // We will enforce enum on app level
    table.text('state').notNullable().defaultTo(GQLTaskState.open);
  });
  await createUpdatedAtTrigger(knex, 'tasks');
}

export async function down(knex: Knex): Promise<void> {
  await dropUpdatedAtTrigger(knex, 'tasks');
  await knex.schema.dropTable('tasks');
}
