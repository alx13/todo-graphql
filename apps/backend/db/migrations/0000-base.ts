import * as Knex from 'knex';
import { defineUpdateAtProcedure } from '../helpers';


export async function up(knex: Knex): Promise<void> {
  await defineUpdateAtProcedure(knex);
}

// we can safely skip that
// tslint:disable-next-line: no-empty
export async function down(_knex: Knex): Promise<void> {
}
