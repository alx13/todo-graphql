// tslint:disable
const path = require('path');

require('ts-node').register({
  files: true,
  project: path.join(__dirname, './tsconfig.json'),
});

const tsConfig = require('../tsconfig.json');
require('tsconfig-paths').register({
  baseUrl: path.join(__dirname, '../src/'),
  paths: tsConfig.compilerOptions.paths,
});

module.exports = require('./config');
