import Knex from 'knex';

export function timestamps(knex: Knex, table: Knex.CreateTableBuilder) {
  table
    .specificType('created_at', 'timestamptz(3)')
    .index()
    .notNullable()
    .defaultTo(knex.fn.now());
  table
    .specificType('updated_at', 'timestamptz(3)')
    .index()
    .notNullable()
    .defaultTo(knex.fn.now());
}

export async function defineUpdateAtProcedure(knex: Knex) {
  await knex.schema.raw(`
    CREATE OR REPLACE FUNCTION generate_updated_at()
    RETURNS TRIGGER AS $$
    BEGIN
      NEW.updated_at = Now();
      RETURN NEW;
    END;
    $$ LANGUAGE 'plpgsql';
  `);
}

export async function createUpdatedAtTrigger(knex: Knex, tableName: string) {
  return knex.schema.raw(knex.raw(
    `
      CREATE TRIGGER last_updated BEFORE UPDATE ON :tableName:
      FOR EACH ROW EXECUTE PROCEDURE generate_updated_at();
    `,
    { tableName }
  ).toString());
}

export async function dropUpdatedAtTrigger(knex: Knex, tableName: string) {
  return knex.schema.raw(knex.raw(
    `
      DROP TRIGGER IF EXISTS last_updated ON :tableName:;
    `,
    { tableName }
  ).toString());
}
