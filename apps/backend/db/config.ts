import 'reflect-metadata';

import { snakeCaseMappers } from 'objection';
import { parseEnv } from '@todo-graphql/env';

const env = parseEnv(process.env);

const development = {
  connection: env.postgres,
  client: 'postgresql',
  pool: {
    min: 2,
    max: 10,
  },
  extension: 'ts',
  migrations: {
    tableName: 'knex_migrations',
    schemaName: 'public',
    directory: './migrations',
  },
  seeds: {
    directory: './seeds',
  },
  loadExtensions: ['.ts'],
  ...snakeCaseMappers,
};

export {
  development,
};
