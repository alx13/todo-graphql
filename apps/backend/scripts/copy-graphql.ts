import Bluebird = require('bluebird');
import * as glob from 'glob';
import * as path from 'path';
import * as fs from 'fs-extra';

async function copyGraphql() {
  const files = glob.sync(
    '**/*.graphql',
    {
      cwd: './src',

    }
  );

  await Bluebird.each(
    files,
    (file) => {
      console.log(file);

      return fs.copy(
        path.join('src', file),
        path.join('dist', file)
      );
    }
  );
}

if (require.main === module) {
  copyGraphql()
  .then(() => {
    process.exit(0);
  })
  .catch((err) => {
    console.error(err);

    process.exit(1);
  });
}
