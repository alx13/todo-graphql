import path from 'path';
import fs from 'fs';
import express from 'express';
import { initCommonRouter } from './common';
import { Env } from '@todo-graphql/env';

export async function initServer(env: Env) {
  const app = express();

  const router = express.Router({
    strict: true,
  });

  router.use(
    '/bundle',
    express.static(path.join(__dirname, '../../dist/client'))
  );
  app.use(router);

  const html = fs.readFileSync(
    path.join(__dirname, '../../dist/client/index.html'),
    'utf-8'
  );

  const commonRouter = initCommonRouter(env, { html });
  app.use(commonRouter);

  return app;
}

