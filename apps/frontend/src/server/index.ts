// tslint:disable: no-console
import { parseEnv } from '@todo-graphql/env';
import { makeSiteUrl } from '@todo-graphql/helpers';

import {
  initServer,
} from './run';

export {
  initCommonRouter,
} from './common';

export {
  initServer,
} from './run';


if (require.main === module) {
  const env = parseEnv(process.env);

  initServer(env)
    .then((app) => {
      app.listen(env.frontend.internal.port);
      console.log(`server is listening on ${makeSiteUrl(env.frontend.external)}`);
    })
    .catch((err) => {
      console.error('Init server failed', { stack: err.stack });
      process.exit(1);
    });
}
