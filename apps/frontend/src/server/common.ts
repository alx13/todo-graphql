import express from 'express';
import { Env } from '@todo-graphql/env';
import { makeSiteUrl } from '@todo-graphql/helpers';
import { compile as compileHbs } from 'handlebars';
import { SchConfig } from '../config';

export function getConfig(
  env: Env,
  _req: express.Request
): SchConfig {
  return {
    graphqlEndpoint: `${makeSiteUrl(env.backend.external)}${env.backend.graphqlEndpoint}`,
  };
}

export function initCommonRouter(env: Env, state: { html?: string }) {
  const router = express.Router({
    strict: true,
  });

  router.get(
    '/',
    [
      (
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
      ) => {
        if (req.method !== 'GET') {
          return next();
        }

        if (!state.html) {
          res.sendStatus(500);

          return;
        }

        const config = getConfig(env, req);
        const context = {
          schConfig: JSON.stringify(config),
        };
        res.send(compileHbs(state.html)(context));
      },
    ]
  );

  return router;
}
