// tslint:disable
export type Maybe<T> = T | null | undefined;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type GQLMutation = {
  __typename?: "Mutation";
  createTask: GQLTask;
  updateTask: GQLTask;
  changeTaskState: GQLTask;
};

export type GQLMutationCreateTaskArgs = {
  input: GQLTaskInput;
};

export type GQLMutationUpdateTaskArgs = {
  id: Scalars["ID"];
  input: GQLTaskInput;
};

export type GQLMutationChangeTaskStateArgs = {
  id: Scalars["ID"];
  state: GQLTaskState;
};

export type GQLQuery = {
  __typename?: "Query";
  tasks: Array<GQLTask>;
  taskById?: Maybe<GQLTask>;
};

export type GQLQueryTaskByIdArgs = {
  id: Scalars["ID"];
};

export type GQLTask = {
  __typename?: "Task";
  id: Scalars["ID"];
  title: Scalars["String"];
  description?: Maybe<Scalars["String"]>;
  state: GQLTaskState;
  canGo: Array<GQLTaskState>;
};

export type GQLTaskInput = {
  /**  required for createTask  */
  title?: Maybe<Scalars["String"]>;
  description?: Maybe<Scalars["String"]>;
  state?: Maybe<GQLTaskState>;
};

export enum GQLTaskState {
  open = "open",
  pending = "pending",
  closed = "closed"
}
