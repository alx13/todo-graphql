// tslint:disable:no-console
import React, {
  useState,
  useCallback,
} from 'react';
import {
  Mutation,
  MutationOptions,
  FetchResult,
} from 'react-apollo';
import gql from 'graphql-tag';

import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import TextField from '@material-ui/core/TextField';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';

import {
  GQLTask,
  GQLMutationUpdateTaskArgs,
  GQLMutationCreateTaskArgs,
} from '#/gql-types';

import {
  NEW_TASK_ID,
  taskFragment,
} from './constants';
import { TaskState } from './task-state';

export const createTaskMutation = gql`
mutation createTaskMutation($input: TaskInput!) {
  task: createTask(input: $input) {
    ...taskFragment
  }
}
${taskFragment}
`;

export const updateTaskMutation = gql`
mutation updateTaskMutation($id: ID!, $input: TaskInput!) {
  task: updateTask(id: $id, input: $input) {
    ...taskFragment
  }
}
${taskFragment}
`;

type ChangeEditableTask = React.Dispatch<React.SetStateAction<GQLTask>>;

export interface SaveOrCreateButtonProps {
  close?(): void;
  changeEditableTask?: ChangeEditableTask;
  task: GQLTask;
}
const SaveOrCreateButton: React.FC<SaveOrCreateButtonProps> = ({
  task,
  close,
  changeEditableTask,
}) => {
  const isNew = task.id === NEW_TASK_ID;

  const mutation = isNew
    ? createTaskMutation
    : updateTaskMutation;

  const variables = {
    id: isNew ? undefined : task.id,
    input: {
      title: task.title,
      description: task.description,
      state: task.state,
    },
  } as unknown as GQLMutationCreateTaskArgs | GQLMutationUpdateTaskArgs;

  const mutationOptions: MutationOptions<{task: GQLTask}, typeof variables> = {
    variables,
    ...isNew
      ? {
        refetchQueries: [
          'tasksQuery',
        ],
      }
      : undefined,
  };

  const label = isNew
    ? 'Create'
    : 'Update';

  function processFetchResult(result: void | FetchResult<{ task: GQLTask }>) {
    if (!changeEditableTask) {
      return;
    }

    if (!result) {
      return;
    }

    const { data } = result;
    if (!data) {
      return;
    }

    const { task: fetchedTask } = data;

    changeEditableTask(fetchedTask);
  }

  return (
    <Mutation<{ task: GQLTask }, typeof variables>
      mutation={mutation}
    >
      {(mutationFn) => (
        <Button
          color="primary"
          onClick={() => {
            mutationFn(mutationOptions)
              .then(processFetchResult)
              .then(() => {
                if (!close) {
                  return;
                }
                close();
              })
              .catch((err) => console.error(err));
          }}
        >{label}</Button>
      )}
    </Mutation>
  );
};

interface TaskEditDialogProps {
  task: GQLTask;
  onClose?(): void;
}
export const TaskEditDialog: React.FC<TaskEditDialogProps> = ({
  task,
  onClose,
}) => {
  const [editableTask, changeEditableTask] = useState<GQLTask>({ ...task });

  const handleChange = useCallback(
    (name: keyof GQLTask) => (event: React.ChangeEvent<HTMLInputElement>) => {
      if (!editableTask) {
        return;
      }

      changeEditableTask({
        ...editableTask,
        [name]: event.target.value,
      });
    },
    [editableTask, changeEditableTask]
  );

  return (
    <Dialog
      open={true}
      onClose={onClose}
      maxWidth="md"
      fullWidth
    >
      <DialogTitle>Edit task "{editableTask.id}"</DialogTitle>
      <DialogContent>
        <Grid
          container
          direction="column"
          spacing={2}
        >
          <Grid item xs={12}>
            <TaskState
              task={editableTask}
              onChange={handleChange('state')}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              autoFocus
              fullWidth
              label="Title"
              id="title"
              value={editableTask.title}
              onChange={handleChange('title')}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              fullWidth
              multiline
              rows={3}
              label="Description"
              id="description"
              value={editableTask.description || ''}
              onChange={handleChange('description')}
            />
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} color="secondary">
          Cancel
          </Button>
        <SaveOrCreateButton
          close={onClose}
          changeEditableTask={changeEditableTask}
          task={editableTask}
        />
      </DialogActions>
    </Dialog>
  );
};
