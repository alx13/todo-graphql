// tslint:disable:react-a11y-role-has-required-aria-props
// tslint:disable:no-console

import React from 'react';
import {
  Mutation,
} from 'react-apollo';
import gql from 'graphql-tag';

import NativeSelect from '@material-ui/core/NativeSelect';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';

import {
  GQLTaskState,
  GQLTask,
  GQLMutationChangeTaskStateArgs,
} from '#/gql-types';

import { taskFragment } from './constants';

export interface TaskStateProps {
  task: GQLTask;
  displayLabel?: boolean;
  onChange?(e: React.ChangeEvent<{ value: unknown }>): void;
}

export const TaskState: React.FC<TaskStateProps> = ({ task, displayLabel, onChange }) => {
  const { state, canGo } = task;

  function isOptionDisabled(value: GQLTaskState) {
    return state !== value && !canGo.includes(value);
  }

  return (
    <React.Fragment>
      <FormControl>
        {
          displayLabel
            ? (<InputLabel>State</InputLabel>)
            : null
        }
        <NativeSelect
          value={state}
          onChange={onChange}
        >
          {
            Object.values(GQLTaskState).map((value) => (
              <option
                disabled={isOptionDisabled(value)}
                key={value}
                value={value}
              >
                {value}
              </option>
            ))
          }
        </NativeSelect>
      </FormControl>
    </React.Fragment>
  );
};

export const changeTaskStateMutation = gql`
mutation changeTaskStateMutation($id: ID!, $state: TaskState!) {
  task: changeTaskState(id: $id, state: $state) {
    ...taskFragment
  }
}
${taskFragment}
`;

export const TaskStateChanger: React.FC<{ task: GQLTask }> = ({ task }) => (
    <Mutation<{ task: GQLTask }, GQLMutationChangeTaskStateArgs>
      mutation={changeTaskStateMutation}
    >
      {(changeTaskState) => (
        <TaskState
        task={task}
        onChange={(e) => {
          const newState = e.target.value as GQLTaskState;
          if (task.state === newState) {
            return;
          }

          changeTaskState({
            variables: {
              id: task.id,
              state: newState,
            },
          })
          .catch((err) => console.error(err));
        }}
      />
    )}
  </Mutation>
);
