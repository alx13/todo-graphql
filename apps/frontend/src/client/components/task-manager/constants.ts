import gql from 'graphql-tag';

export const NEW_TASK_ID = 'new';

export const taskFragment = gql`
fragment taskFragment on Task {
  id
  state
  title
  description
  canGo
}
`;
