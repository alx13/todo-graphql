import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import IconButton from '@material-ui/core/IconButton';
import Edit from '@material-ui/icons/Edit';

import {
  GQLTask,
} from '../../gql-types';

import { TaskStateChanger } from './task-state';

export interface TaskListItemProps {
  task: GQLTask;
  onEdit?(): void;
}
export const TaskListItem: React.FC<TaskListItemProps> = ({ task, onEdit }) => (
  <ListItem divider>
    <ListItemText
      inset
      primary={task.title}
      secondary={task.description}
    />
    <ListItemSecondaryAction>
      <TaskStateChanger task={task} />
      <IconButton onClick={onEdit}>
        <Edit />
      </IconButton>
    </ListItemSecondaryAction>
  </ListItem>
);
