import React, {
  useState,
  useCallback,
} from 'react';
import gql from 'graphql-tag';
import {
  Query,
} from 'react-apollo';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';

import {
  GQLQuery,
  GQLTaskState,
  GQLTask,
} from '#/gql-types';

import {
  NEW_TASK_ID,
  taskFragment,
} from './constants';
import { TaskListItem } from './task-item';
import { TaskEditDialog } from './task-edit';

export const tasksQuery = gql`
query tasksQuery {
  tasks {
    ...taskFragment
  }
}
${taskFragment}
`;

export const TaskManager: React.FC = () => {
  const [taskToEdit, setTaskToEdit] = useState<GQLTask | null>(null);

  const onDialogClose = useCallback(
    () => {
      setTaskToEdit(null);
    },
    [setTaskToEdit]
  );

  const onTaskToEditChange = useCallback(
    (task: GQLTask) => () => {
      setTaskToEdit(task);
    },
    [taskToEdit, taskToEdit]
  );

  // it's may be better to create
  // the new temporary instance of the task on backend
  // and then fetch to avoid duplicating logic
  const stubTask = {
    id: NEW_TASK_ID,
    title: '',
    description: '',
    state: GQLTaskState.open,
    canGo: [GQLTaskState.pending, GQLTaskState.closed],
  };

  return (
    <Query<GQLQuery> query={tasksQuery}>
      {({ data }) => {
        if (!data) {
          return null;
        }

        const { tasks } = data;

        if (!tasks) {
          return null;
        }

        return (
          <React.Fragment>
            <Card>
              <CardContent>
                <Grid
                  container
                  direction="row"
                  justify="space-between"
                  alignContent="center"
                >
                  <Grid item>
                    <Typography variant="h5" component="h1">
                      Tasks
                    </Typography>
                  </Grid>
                  <Grid item>
                    <Button
                      color="primary"
                      onClick={onTaskToEditChange(stubTask)}
                    >Create</Button>
                  </Grid>
                </Grid>
                <List>
                  {tasks
                    .map((task) => (
                      <TaskListItem
                        key={task.id}
                        task={task}
                        onEdit={onTaskToEditChange(task)}
                      />
                    ))}
                </List>
              </CardContent>
            </Card>
            {
              taskToEdit
                ? (
                  <TaskEditDialog
                    task={taskToEdit}
                    onClose={onDialogClose}
                  />
                )
                : null
            }
          </React.Fragment>
        );
      }}
    </Query>
  );
};
