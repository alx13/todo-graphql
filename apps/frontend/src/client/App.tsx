import React from 'react';

import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';

import {
  TaskManager,
} from './components/task-manager';

export const App: React.FC = () => (
  <React.Fragment>
    <Container component="main" maxWidth="md">
      <CssBaseline />
      <TaskManager />
    </Container>
  </React.Fragment>
);
