import React from 'react';
import ReactDOM from 'react-dom';
import { ApolloProvider } from 'react-apollo';

import { initApolloClient } from './apollo-client';

// tslint:disable-next-line: no-import-side-effect
import './index.css';
import { App } from './App';


const config = window.TDG_CONFIG;
const client = initApolloClient(config);

ReactDOM.render(
  (
    <ApolloProvider client={client}>
      <App />
    </ApolloProvider>
  ),
  document.getElementById('root')
);
