import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';
import { SchConfig } from '../config';

export function initApolloClient(config: SchConfig) {
  const cache = new InMemoryCache();
  const link = new HttpLink({
    uri: config.graphqlEndpoint,
  });

  return new ApolloClient({
    cache,
    link,
  });
}
