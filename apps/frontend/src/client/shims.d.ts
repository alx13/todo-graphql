import { SchConfig } from '../config';

declare global {
  interface Window {
    TDG_CONFIG: SchConfig;
  }
}
