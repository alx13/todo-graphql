// tslint:disable:no-console

import path from 'path';
import express from 'express';
import webpack from 'webpack';
import WebpackHotClinet from 'webpack-hot-client';
import WebpackDevMiddleware from 'webpack-dev-middleware';

import { makeSiteUrl } from '@todo-graphql/helpers';
import { Env, parseEnv } from '@todo-graphql/env';

import { getConfig } from './webpack';

import { initCommonRouter } from '../src/server';

export class State {
  public html?: string;
  public constructor(bundles?: {
    html?: string;
  }) {
    if (!bundles) {
      return;
    }

    this.html = bundles.html;
  }
}


export async function getWebpackHotClient(
  compiler: webpack.Compiler,
  options: WebpackHotClinet.Options
) {
  return new Promise((resolve) => {
    // FIXME: error in typings
    const client = WebpackHotClinet(compiler, options) as any;
    const { server } = client;

    server.on('listening', () => resolve(client));
  });
}

export async function initRenderer(env: Env, state: State) {
  const clientConfig = getConfig(false);
  const clientCompiler = webpack(clientConfig);

  const clientOutput = clientConfig.output;
  if (!clientOutput || !clientOutput.path || !clientOutput.publicPath) {
    throw new Error('wrong configuration');
  }
  const {
    path: outputPath,
    publicPath,
  } = clientOutput;

  const webpackHotClient = await getWebpackHotClient(
    clientCompiler,
    {
      https: !env.frontend.external.useHttp,
      host: env.frontend.external.host,
    }
  );

  const webpackDevMiddleware = WebpackDevMiddleware(
    clientCompiler,
    {
      publicPath,
      index: false,
      headers: {
        'Access-Control-Allow-Origin': makeSiteUrl(env.frontend.external),
      },
    }
  );

  let inited = false;

  return new Promise((resolve) => {
    clientCompiler
      .hooks.done.tap(
        'update-html',
        (stats: webpack.Stats) => {
          const jsonStats = stats.toJson();
          jsonStats.errors.forEach((err: string) => console.error(err));
          jsonStats.warnings.forEach((err: string) => console.warn(err));
          if (jsonStats.errors.length) {
            resolve();

            return;
          }

          const html = webpackDevMiddleware
            .fileSystem
            .readFileSync(
              path.join(outputPath, 'index.html'),
              'utf-8'
            );

          state.html = html;

          if (!inited) {
            inited = true;
            resolve();
          }
        }
      );
  })
    .then(() => ({
      clientCompiler,
      webpackDevMiddleware,
      webpackHotClient,
    }));
}

export async function initDevServer(env: Env) {
  const app = express();

  const router = express.Router({
    strict: true,
  });

  const state = new State();

  const { webpackDevMiddleware } = await initRenderer(env, state);
  router.get('/bundle/*', webpackDevMiddleware);
  app.use(router);

  const commonRouter = initCommonRouter(env, state);
  app.use(commonRouter);

  return app;
}

if (require.main === module) {
  const env = parseEnv(process.env);

  initDevServer(env)
    .then((app) => {
      app.listen(env.frontend.internal.port);
      const url = makeSiteUrl(env.frontend.external);

      console.log(`🚀 dev server is listening on ${url}`);
    })
    .catch((err) => {
      console.error(err);
      process.exit(1);
    });
}
