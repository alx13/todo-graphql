declare module 'postcss-safe-parser';
declare module 'react-dev-utils/ModuleNotFoundPlugin';
declare module 'case-sensitive-paths-webpack-plugin';
declare module 'react-dev-utils/WatchMissingNodeModulesPlugin';
declare module 'mini-css-extract-plugin';
declare module 'postcss-normalize';
declare module 'react-dev-utils/getCSSModuleLocalIdent';
declare module 'friendly-errors-webpack-plugin';
