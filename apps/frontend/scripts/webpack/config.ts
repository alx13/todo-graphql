import path from 'path';
import {
  Configuration,
  Plugin,
  DefinePlugin,
  NewLoader,
  ProgressPlugin,
} from 'webpack';
import isWsl from 'is-wsl';
import TsconfigPathPlugin from 'tsconfig-paths-webpack-plugin';
import TerserPlugin from 'terser-webpack-plugin';
import OptimizeCSSAssetsPlugin from 'optimize-css-assets-webpack-plugin';
import safePostCssParser from 'postcss-safe-parser';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import ModuleNotFoundPlugin from 'react-dev-utils/ModuleNotFoundPlugin';
import CaseSensitivePathsPlugin from 'case-sensitive-paths-webpack-plugin';
import WatchMissingNodeModulesPlugin from 'react-dev-utils/WatchMissingNodeModulesPlugin';
import FriendlyErrors from 'friendly-errors-webpack-plugin';
import { BundleAnalyzerPlugin } from 'webpack-bundle-analyzer';

import MiniCssExtractPlugin from 'mini-css-extract-plugin';

// TODO: Copyright notice because some code was copied from create-react-app

export function getOptimization(isProduction: boolean): Configuration['optimization'] {
  return {
    minimize: isProduction,
    minimizer: [
      new TerserPlugin({
        terserOptions: {
          parse: {
            ecma: 8,
          },
          compress: {
            // FIXME: typings are wrong
            // @ts-ignore
            ecma: 5,
            warnings: false,
            comparisons: false,
            inline: 2,
          },
          mangle: {
            safari10: true,
          },
          output: {
            ecma: 5,
            comments: false,
            ascii_only: true,
          },
        },
        parallel: !isWsl,
        // Enable file caching
        cache: true,
        // expenive so we turn it off for now
        sourceMap: false,
      }),
      // This is only used in production mode
      new OptimizeCSSAssetsPlugin({
        cssProcessorOptions: {
          parser: safePostCssParser,
          map: false,
        },
      }),
    ],
    // Automatically split vendor and commons
    // https://twitter.com/wSokra/status/969633336732905474
    // https://medium.com/webpack/webpack-4-code-splitting-chunk-graph-and-the-splitchunks-optimization-be739a861366
    splitChunks: {
      chunks: 'all',
      // name: false,
    },
    // Keep the runtime chunk separated to enable long term caching
    // https://twitter.com/wSokra/status/969679223278505985
    runtimeChunk: true,
  };
}

export function getStyleLoaders(
  isProduction: boolean,
  shouldUseRelativeAssetPaths: boolean,
  cssOptions: {
    importLoaders: number;
    modules?: boolean;
    getLocalIdent?: unknown;
  },
  preProcessor?: string
): NewLoader[] {
  return [
    isProduction && require.resolve('style-loader'),
    isProduction && {
      loader: MiniCssExtractPlugin.loader,
      options: shouldUseRelativeAssetPaths ? { publicPath: '../../' } : {},
    },
    {
      loader: require.resolve('css-loader'),
      options: cssOptions,
    },
    preProcessor
      ? {
        loader: require.resolve(preProcessor),
        options: {
          sourceMap: false,
        },
      }
      : undefined,
  ].filter(Boolean) as NewLoader[];
}


export function getModule(isProduction: boolean): Configuration['module'] {
  const tslintLoader: NewLoader = {
    loader: 'tslint-loader',
    options: {
      configFile: path.join(
        __dirname,
        '../../src/client/tslint.json'
      ),
      emitErrors: false,
      failOnHint: false,
      typeCheck: true,
      tsConfigFile: path.join(
        __dirname,
        '../../src/client/tsconfig.json'
      ),
    },
  };

  const tsLoader: NewLoader = {
    loader: 'ts-loader',
    options: {
      configFile: path.join(
        __dirname,
        '../../src/client/tsconfig.json'
      ),
      transpileOnly: false,
      happyPackMode: true,
    },
  };

  return {
    strictExportPresence: true,
    rules: [
      { parser: { requireEnsure: false } },
      {
        oneOf: [
          {
            test: /\.(ts|tsx)$/,
            exclude: /node_modules/,
            rules: [
              {
                use: [
                  tsLoader,
                ],
              },
              {
                enforce: 'pre',
                use: isProduction
                  ? [tslintLoader]
                  : [],
              },
            ],
          },
          {
            test: /\.(png|jpe?g|gif|webp|svg)(\?.*)?$/,
            use: [
              {
                loader: 'url-loader',
                options: {
                  limit: 4096,
                  name: 'img/[name].[hash:8].[ext]',
                },
              },
            ],
          },
          {
            test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/i,
            use: [
              {
                loader: 'url-loader',
                options: {
                  limit: 4096,
                  name: 'fonts/[name].[hash:8].[ext]',
                },
              },
            ],
          },
          {
            test: /\.css$/,
            sideEffects: true,
            oneOf: [
              {
                use: [
                  ...getStyleLoaders(
                    isProduction,
                    false,
                    {
                      importLoaders: 1,
                    }
                  ),
                ],
              },
            ],
          },
          {
            loader: require.resolve('file-loader'),
            exclude: [
              /index\.html\.hbs$/,
              /\.(js|mjs|jsx|ts|tsx)$/,
              /\.html$/,
              /\.json$/,
            ],
            options: {
              name: 'static/media/[name].[hash:8].[ext]',
            },
          },
        ],
      },
    ],
  };
}

function getProgressProdHandler() {
  const cache = new Set<string>();

  return (percentage: number, message: string) => {
    const formattedMessage = `${(percentage * 100).toFixed()}% ${message}`;
    if (cache.has(formattedMessage)) {
      return;
    }
    cache.add(formattedMessage);

    // tslint:disable-next-line: no-console
    console.log(`${(percentage * 100).toFixed()}% ${message}`);
  };
}

function getPlugins(
  isProduction: boolean,
  enableBundleAnalyzer?: boolean
): Configuration['plugins'] {
  const mode = isProduction
      ? 'production'
      : 'development';

  return [
    new DefinePlugin(
      {
        'process.env': {
          NODE_ENV: `"${mode}"`,
        },
      }
    ),
    new HtmlWebpackPlugin({
      inject: true,
      template: path.join(__dirname, '../../public/index.html.hbs'),
      ...isProduction
        ? {
          minify: {
            removeComments: true,
            collapseWhitespace: true,
            removeRedundantAttributes: true,
            useShortDoctype: true,
            removeEmptyAttributes: true,
            removeStyleLinkTypeAttributes: true,
            keepClosingSlash: true,
            minifyJS: true,
            minifyCSS: true,
            minifyURLs: true,
          },
        }
        : undefined,
    }),
    new ModuleNotFoundPlugin(path.join(__dirname, '../../src/client')),
    isProduction
      ? undefined
      : new CaseSensitivePathsPlugin(),
    isProduction
      ? undefined
      : new WatchMissingNodeModulesPlugin(),
    isProduction
      ? new MiniCssExtractPlugin({
        filename: 'css/[name].[contenthash:8].css',
        chunkFilename: 'css/[name].[contenthash:8].css',
      })
      : undefined,
    new ProgressPlugin(
      isProduction
        ? getProgressProdHandler()
        : undefined
    ),
    new FriendlyErrors({
      clearConsole: false,
    }),
    isProduction && enableBundleAnalyzer
      ? new BundleAnalyzerPlugin()
      : undefined,
  ].filter((plugin): plugin is Plugin => Boolean(plugin));
}

export function getConfig(isProduction: boolean): Configuration {
  const mode = isProduction
      ? 'production'
      : 'development';

  return {
    mode,
    bail: isProduction,
    context: path.join(__dirname, '../../src/client'),
    devtool: isProduction
      ? 'source-map'
      : 'cheap-module-eval-source-map',
    target: 'web',
    output: {
      filename: isProduction
        ? 'js/[name].[chunkhash:8].js'
        : 'js/[name].js',
      path: path.join(__dirname, '../../dist/client'),
      publicPath: '/bundle/',
    },
    entry: [
      path.join(__dirname, '../../src/client/index.tsx'),
    ],
    resolve: {
      plugins: [
        new TsconfigPathPlugin({
          configFile: path.join(
            __dirname,
            '../../src/client/tsconfig.json'
          ),
        }),
      ],
      symlinks: true,
      alias: {
      },
      extensions: [
        '.js',
        '.jsx',
        '.json',
        '.ts',
        '.tsx',
      ],
    },
    node: {
      setImmediate: false,
      module: 'empty',
      dgram: 'empty',
      dns: 'mock',
      fs: 'empty',
      http2: 'empty',
      net: 'empty',
      tls: 'empty',
      child_process: 'empty',
    },
    optimization: getOptimization(isProduction),
    plugins: getPlugins(isProduction),
    module: getModule(isProduction),

    performance: false,
  };
}
