// tslint:disable:no-console
import webpack from 'webpack';

import {
  getConfig,
} from './webpack';

export async function build(config: webpack.Configuration) {
  console.log(`Building with mode "${config.mode}"`);

  return new Promise((resolve, reject) => {
    webpack(
      config,
      (err, stats) => {
        if (err) {
          return reject(err);
        }
        if (stats.hasErrors()) {
          console.log(stats.toString());
          reject('some errors');

          return;
        }

        return resolve();
      }
    );
  });
}

if (require.main === module) {
  process.env.NODE_ENV = 'production';

  const config = getConfig(true);

  build(config)
    .catch((err) => {
      console.error(err);
      process.exit(1);
    });
}

