## Build and run
To build and start project with docker-compose
```bash
# starts the server
make docker-compose-up
# rebuilds contatiners if need
make docker-compose-build
```

Default URLs:
- frontend [http://localhost:8081](http://localhost:8081) 
- graphql playground [http://localhost:8080/graphql](http://localhost:8080/graphql) 

## Project structure
Project is a monorepository. To manage it you will need [pnpm](https://pnpm.js.org) tool.

```bash
apps
└── backend         # GraphQL server
└── frontend        # Fronted React app + server to serve it
env  
└── local.env.yaml  # Env config for developement
libraries           # Libraries and tools used in applications
└── env             # Env parser with TypeScript typings for resulting Env object
└── env-yaml-exec   # Tool to run application with Env readed from YAML files
└── helpers         # Common functions to use in applications
└── tslint-config   # Config for tslint
services            # Docker-compose based services to when developing
└── postgres        # PostgreSQL database
tools  
└── api-gen         # Configured graphql-codegen to generate typings from GQL Schema
└── testing         # Configured graphql-inspector to validate GQL Schema usage
```

Noticeable libraries used in the project:
- [graphql-modules](https://graphql-modules.com/)
- [graphql-code-generator](https://graphql-code-generator.com)
- [graphql-inspector](https://graphql-inspector.com/)
- [kexec](https://github.com/jprichardson/node-kexec)

All servers configuration is done by using environment variables,
to conform with [twelve-factor app](https://12factor.net/) methodology.


## Development
You will need to start with installing [pnpm](https://pnpm.js.org).

To initially install dependencies run `pnpm recursive install` in the project root

To start development you will need to run:
```bash
# postgres using docker-compose
make start-postgres
# GraphQL API backend with ts-node-dev
make start-backend-dev
# frontend server with webpack-dev-middleware/webpack-hot-client
make start-frontend-dev
# watch *.graphql files in apps/backend/src and regenerate typings
make start-gql-codegen

# migrate db
cd apps/backend && make knex-migrate
```

Testing
```bash
# run backend e2e tests NB: needs running postgres
make test-backend-e2e
# run backend spec tests
make test-backend-spec
# run all spec tests
make test-spec
```

To test API usage against schema:
```bash
cd tools/testing

 # to copy current Schema as previous for retrospection purposes
make update-previous-schema
# validate frontend's GrahQL documents against Schema
make validate
# show coverage using frontend's GraphQL documents against Schema
make coverage
# check for breaking changes in current Schema against previous one
make diff
```

## To do
- Very unoptimzed dockerfiles. Should use multi-FROM technique to achive smaller contatiner
- Build chain for the frontend is very simplified
    - Need to add things like postcss, css-modules, etc.
    - Split bundle in to more meaningful parts
    - Optimize bundle size
- There is no UI tests
- You can run API tests manually, but there should be a separate script for that with proper
  setup and teardown
- UI kinda sucks
- Some parts of the code is not too clean enough
- Need more extensive e2e testing for different scenarios
