FROM node:10-alpine

ENV DEBIAN_FRONTEND=noninteractive \
    LANG=en_US.UTF-8 \
    LANGUAGE=en_US:en \
    LC_ALL=en_US.UTF-8 \
    DUMB_INIT_VERSION="1.2.2"

RUN set -x \
  && apk add \
    curl \
    python \
    build-base \
  && curl -L \
    -o /usr/local/bin/dumb-init \
    https://github.com/Yelp/dumb-init/releases/download/v${DUMB_INIT_VERSION}/dumb-init_${DUMB_INIT_VERSION}_amd64 \
  &&  chmod +rx /usr/local/bin/dumb-init

WORKDIR /opt/app/
# because without token scoped packages don't work
ENV \
    NPM_TOKEN="00000000-0000-0000-0000-000000000000" \
    PNPM_VERSION="3.5.3"

RUN set -x \
    && npm set send-metrics=false \
    && npm set update-notifier=false \
    && npm set package-lock=true \
    && npm set global-style=true \
    && npm set progress=false \
    && npm set loglevel=notice \
    && npm config set store /opt/pnpm-store \
    && echo "//registry.npmjs.org/:_authToken=${NPM_TOKEN}" >> /root/.npmrc \
    && npm install -g "pnpm@${PNPM_VERSION}"

COPY \
    .npmrc \
    package.json \
    pnpm-workspace.yaml \
    tsconfig.base.json \
    ./

ENV \
  TDG__SERVICE_NAME="backend"

RUN set -x \
  && mkdir \
    apps \
    libraries \
    tools \
    types

COPY libraries libraries
COPY tools tools
COPY \
 apps/${TDG__SERVICE_NAME}/package.json \
 apps/${TDG__SERVICE_NAME}/pnpm-lock.yaml \
 apps/${TDG__SERVICE_NAME}/

RUN set -x \
   && pnpm recursive install \
    --frozen-shrinkwrap

COPY apps/${TDG__SERVICE_NAME} apps/${TDG__SERVICE_NAME}

ENV \
  NODE_ENV="production"

RUN set -x \
  && make -C ./apps/${TDG__SERVICE_NAME} build

WORKDIR /opt/app/apps/${TDG__SERVICE_NAME}
RUN ls -la
RUN pwd
CMD [ "dumb-init", "node", "--require", "./tsconfig-paths.js", "--require", "./source-map-support.js", "dist" ]

USER nobody
RUN du -hs /opt/app
